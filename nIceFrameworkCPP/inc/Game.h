#pragma once
#include <GL/glew.h>
#include "Window.h"
#include "gfx/DeferredTarget.h"

namespace nif
{
	class Game
	{
	public:
		static void Start();
		static Window *Screen();
		static void BindTarget(const DeferredTarget *target);

	private:
		Game();
		static void ErrorCallback(int, const char*);

	private:
		static Window *window;
	};
}
