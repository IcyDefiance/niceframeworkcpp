#pragma once
#include <GLFW/glfw3.h>
#include "nifmath.h"

namespace nif
{
	class Window
	{
	public:
		Window();
		~Window();
		void MakeCurrent();
		bool ShouldClose();
		void SwapBuffers();
		void KeyPress(int, int, int, int);
		ivec2 Size();

	private:
		static void Window::KeyCallback(GLFWwindow*, int, int, int, int);

	private:
		GLFWwindow *window;
		ivec2 size;
	};
}
