#pragma once
#include "nifmath.h"

namespace nif
{
	class Camera
	{
	public:
		Camera(const float width, const float height, const float minRange, const float maxRange, const float fieldOfView);
		~Camera();
		mat4 View();
		mat4 Projection();

	private:
		float width;
		float height;
		float minr;
		float maxr;
		float fov;
	};
}
