#pragma once
#include "gfx/Texture.h"

namespace nif
{
	class DeferredTarget
	{
	public:
		DeferredTarget(const int width, const int height);
		~DeferredTarget();

		unsigned int Id() const;
		const Texture& Diffuse() const;
		const Texture& Position() const;
		const Texture& Normal() const;
		const Texture& Specular() const;

	private:
		DeferredTarget(const DeferredTarget&);

	private:
		Texture dif;
		Texture pos;
		Texture nor;
		Texture spec;
		unsigned int rb;
		unsigned int fb;
	};
}
