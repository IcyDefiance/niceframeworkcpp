#pragma once
#include <vector>

namespace nif
{
	class Mesh
	{
	public:
		Mesh(const std::vector<float> &positions, const std::vector<float> &normals, const std::vector<float> &texcoords, const std::vector<unsigned int> &indices);
		~Mesh();
		unsigned int VAO() const;
		int ElementCount() const;

	private:
		Mesh(const Mesh&);

	private:
		unsigned int vao;
		unsigned int vbo;
		unsigned int ebo;
		int ecount;
	};
}
