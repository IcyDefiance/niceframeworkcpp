#pragma once
#include <vector>
#include "gfx/Model.h"
#include "gfx/PointLight.h"

namespace nif
{
	class MeshBatch
	{
	public:
		MeshBatch();
		~MeshBatch();
		void Add(const Model &model);
		void Add(const PointLight &light);
		void Draw();

	private:
		std::vector<const Model*> toDraw;
		std::vector<const PointLight*> pointLights;
	};
}
