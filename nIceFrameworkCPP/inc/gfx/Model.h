#pragma once
#include <string>
#include "gfx/Mesh.h"

namespace nif
{
	class Model
	{
	public:
		Model(const std::string &filename);
		~Model();
		const std::vector<Mesh*>& Meshes() const;

	private:
		Model(const Model&);

	private:
		std::vector<Mesh*> meshes;
	};
}