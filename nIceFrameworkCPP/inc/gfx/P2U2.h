#pragma once
#include "nifmath.h"

namespace nif
{
	struct P2U2
	{
		vec2 pos;
		vec2 uv;

		P2U2(const vec2 position, const vec2 texcoord);
	};
}
