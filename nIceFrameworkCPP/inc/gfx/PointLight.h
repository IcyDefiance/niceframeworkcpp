#pragma once
#include "nifmath.h"

namespace nif
{
	struct PointLight
	{
		vec3 Position;
		float Range;

		PointLight();
		PointLight(const vec3 pos, const float range);
	};
}
