#pragma once
#include <string>
#include <unordered_map>
#include "nifmath.h"

namespace nif
{
	class Shader
	{
	public:
		Shader(const std::string &vertSrc, const std::string &fragSrc);
		~Shader();
		void MakeActive();
		void SetUniform(const std::string &name, const float val);
		void SetUniform(const std::string &name, const int val);
		void SetUniform(const std::string &name, const vec2 &val);
		void SetUniform(const std::string &name, const ivec2 &val);
		void SetUniform(const std::string &name, const vec3 &val);
		void SetUniform(const std::string &name, const vec4 &val);
		void SetUniform(const std::string &name, const mat4 &val, const bool transpose = false);

	private:
		int UniformLocation(const std::string &name);
		int ShaderParam(unsigned int shader, int param);
		int ProgramParam(unsigned int shader, int param);
		std::string ShaderLog(unsigned int shader);
		std::string ProgramLog(unsigned int shader);

	private:
		unsigned int vertShader;
		unsigned int fragShader;
		unsigned int program;
	};
}
