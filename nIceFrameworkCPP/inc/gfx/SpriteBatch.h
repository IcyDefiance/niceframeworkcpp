#pragma once
#include "gfx/Shader.h"
#include "Texture.h"
#include "nifmath.h"
#include <vector>

namespace nif
{
	class SpriteBatch
	{
	private:
		struct Sprite
		{
			const Texture *Source;
			const vec2 Position;

			Sprite(const Texture &tex, const vec2 pos);
		};

	public:
		SpriteBatch();
		~SpriteBatch();
		void Add(const Texture &texture, const vec2 position);
		void Draw();

	private:
		std::vector<Sprite> toDraw;
	};
}
