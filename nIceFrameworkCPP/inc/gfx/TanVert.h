#pragma once
#include "nifmath.h"

namespace nif
{
	struct TanVert
	{
		vec3 Pos;
		vec3 Nor;
		vec2 UV;
		vec3 Tan;
		vec3 Bitan;

		TanVert(const vec3 pos, const vec3 nor, const vec2 uv, const vec3 tan, const vec3 bitan);
	};
}
