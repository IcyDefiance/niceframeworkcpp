#pragma once
#include <vector>
#include <string>
#include "nifmath.h"

namespace nif
{
	class Texture
	{
	public:
		Texture(const int width, const int height);
		Texture(const int width, const int height, const std::vector<float> &data);
		Texture(const std::string &filename);
		~Texture();

		unsigned int Id() const;
		ivec2 Size() const;

	public:
		static Texture& WhitePixel();
		static Texture& DefaultNormal();
		static Texture& DefaultSpecular();

	private:
		void Init(const int width, const int height);

	private:
		unsigned int id;
		ivec2 size;
	};
}
