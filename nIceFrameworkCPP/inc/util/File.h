#pragma once
#include <string>

namespace nif
{
	namespace File
	{
		std::string ReadAllText(const std::string &filename);
	}
}
