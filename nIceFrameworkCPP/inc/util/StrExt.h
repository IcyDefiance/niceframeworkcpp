#pragma once
#include <string>

namespace nif
{
	namespace StrExt
	{
		std::wstring ToWString(const std::string &in);
	};
}
