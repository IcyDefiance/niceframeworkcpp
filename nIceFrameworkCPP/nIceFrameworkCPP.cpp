// nifCPP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Game.h"

using namespace nif;

void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

int main()
{
	Game::Start();
    return 0;
}

