#version 420

in vec4 wpos;
in vec4 wnormal;
in vec2 uv;
in vec3 tan;
in vec3 bitan;

layout(location = 0) out vec4 diffuse;
layout(location = 1) out vec4 position;
layout(location = 2) out vec4 normal;
layout(location = 3) out vec4 specular;

uniform sampler2D diffMap;
uniform sampler2D normMap;
uniform sampler2D specMap;

void main() {
	diffuse = texture(diffMap, uv);
	position = wpos;
	normal = vec4(mat3(tan, bitan, wnormal) * (texture(normMap, uv).xyz * 2 - 1), 1);
	specular = texture(specMap, uv);
}