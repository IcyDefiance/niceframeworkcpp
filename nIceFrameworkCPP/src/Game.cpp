#include "stdafx.h"
#include "Game.h"
#include <Windows.h>
#include "Window.h"
#include <stdexcept>
#include "gfx/SpriteBatch.h"
#include "gfx/MeshBatch.h"
#include "gfx/Texture.h"
#include "gfx/Model.h"

namespace nif
{
	Window *Game::window;

	void Game::Start()
	{
		if (!glfwInit())
			throw std::runtime_error("Failed glfwInit()");
		glfwSetErrorCallback(ErrorCallback);

		try
		{
			window = new Window();
		}
		catch (const std::runtime_error&)
		{
			glfwTerminate();
			throw;
		}

		if (glewInit() != GLEW_OK)
			throw std::runtime_error("Failed glewInit()");

		glEnable(GL_CULL_FACE);

		SpriteBatch sb;
		MeshBatch mb;
		Texture wp("C:/Users/Icy Defiance/Documents/Code/nIceFeed/nIce-Framework/Test/Test/Content/colors.png");
		Model model("C:/Users/Icy Defiance/Documents/Code/nIce-Framework-Dumb/Test/Test/Content/sphere.obj");
		PointLight light(vec3(5), 50);
		while (!window->ShouldClose())
		{
			glClearColor(0, 0, 0, 1);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			mb.Add(model);
			mb.Add(light);
			mb.Draw();

			//sb.Add(wp, glm::vec2(10, 10));
			//sb.Draw();

			window->SwapBuffers();
			glfwPollEvents();
		}

		delete window;
		glfwTerminate();
	}

	Window * Game::Screen()
	{
		return window;
	}

	void Game::BindTarget(const DeferredTarget *target)
	{
		if (target == nullptr)
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
		else
			glBindFramebuffer(GL_FRAMEBUFFER, target->Id());
	}

	void Game::ErrorCallback(int error, const char *description)
	{
		fputs(description, stderr);
	}
}
