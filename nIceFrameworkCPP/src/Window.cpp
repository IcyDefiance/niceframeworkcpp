#include "stdafx.h"
#include "Window.h"
#include <stdexcept>

namespace nif
{
	Window::Window()
	{
		size = ivec2(1440, 810);

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
		window = glfwCreateWindow(size.x, size.y, "nIce Framework", NULL, NULL);
		if (!window)
			throw std::runtime_error("Failed to create window");

		glfwSetWindowUserPointer(window, this);
		glfwSetKeyCallback(window, KeyCallback);
		MakeCurrent();
	}

	Window::~Window()
	{

	}

	void Window::MakeCurrent()
	{
		glfwMakeContextCurrent(window);
	}

	bool Window::ShouldClose()
	{
		return glfwWindowShouldClose(window) == 1;
	}

	void Window::SwapBuffers()
	{
		glfwSwapBuffers(window);
	}

	void Window::KeyPress(int key, int scancode, int action, int mods)
	{
		if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
			glfwSetWindowShouldClose(window, GL_TRUE);
	}

	ivec2 Window::Size()
	{
		return size;
	}

	void Window::KeyCallback(GLFWwindow *glfwWindow, int key, int scancode, int action, int mods)
	{
		static_cast<Window*>(glfwGetWindowUserPointer(glfwWindow))->KeyPress(key, scancode, action, mods);
	}
}
