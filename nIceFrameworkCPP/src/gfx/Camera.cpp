#include "stdafx.h"
#include "gfx/Camera.h"
#include "nifmath.h"

namespace nif
{
	Camera::Camera(const float width, const float height, const float minRange, const float maxRange, const float fieldOfView) : width(width), height(height), minr(minRange), maxr(maxRange), fov(fieldOfView)
	{
	}

	Camera::~Camera()
	{
	}

	mat4 Camera::View()
	{
		return mat4::lookat(vec3(0), vec3(0, 0, -1), vec3(0, 1, 0));
	}

	mat4 Camera::Projection()
	{
		return mat4::perspectiveFov(fov, width, height, minr, maxr);
	}
}
