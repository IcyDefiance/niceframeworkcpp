#include "stdafx.h"
#include "gfx/DeferredTarget.h"
#include "GL/glew.h"

namespace nif
{
	DeferredTarget::DeferredTarget(const int width, const int height) : dif(width, height), pos(width, height), nor(width, height), spec(width, height)
	{
		glGenRenderbuffers(1, &rb);
		glBindRenderbuffer(GL_RENDERBUFFER, rb);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32F, width, height);

		glGenFramebuffers(1, &fb);
		glBindFramebuffer(GL_FRAMEBUFFER, fb);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, dif.Id(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, pos.Id(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, nor.Id(), 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, spec.Id(), 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rb);

		GLenum buffarray[]{ GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
		glDrawBuffers(4, buffarray);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	DeferredTarget::~DeferredTarget()
	{
		glDeleteRenderbuffers(1, &rb);
		glDeleteFramebuffers(1, &fb);
	}

	unsigned int DeferredTarget::Id() const
	{
		return fb;
	}

	const Texture& DeferredTarget::Diffuse() const
	{
		return dif;
	}

	const Texture& DeferredTarget::Position() const
	{
		return pos;
	}

	const Texture& DeferredTarget::Normal() const
	{
		return nor;
	}

	const Texture& DeferredTarget::Specular() const
	{
		return spec;
	}
}