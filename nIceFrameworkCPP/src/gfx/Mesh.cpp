#include "stdafx.h"
#include "gfx/Mesh.h"
#include "gfx/TanVert.h"
#include "GL/glew.h"
#include "macros.h"

using namespace std;

namespace nif
{
	Mesh::Mesh(const std::vector<float>& positions, const std::vector<float>& normals, const std::vector<float>& texcoords, const std::vector<unsigned int>& indices) : ecount(indices.size())
	{
		size_t vcount = positions.size() / 3;
		vector<TanVert> tanverts;
		tanverts.reserve(vcount);

		for (size_t i = 0; i < vcount; i++)
			tanverts.push_back(TanVert(
				vec3(positions[i * 3], positions[i * 3 + 1], positions[i * 3 + 2]),
				vec3(normals[i * 3], normals[i * 3 + 1], normals[i * 3 + 2]),
				vec2(texcoords[i * 2], texcoords[i * 2 + 1]),
				vec3(0),
				vec3(0)));

		for (size_t i = 0; i < indices.size(); i += 3)
		{
			const vec3 &v0 = tanverts[indices[i + 0]].Pos;
			const vec3 &v1 = tanverts[indices[i + 1]].Pos;
			const vec3 &v2 = tanverts[indices[i + 2]].Pos;
			const vec2 &uv0 = tanverts[indices[i + 0]].UV;
			const vec2 &uv1 = tanverts[indices[i + 1]].UV;
			const vec2 &uv2 = tanverts[indices[i + 2]].UV;

			vec3 dpos1 = v1 - v0;
			vec3 dpos2 = v2 - v0;
			vec2 duv1 = uv1 - uv0;
			vec2 duv2 = uv2 - uv0;

			float r = 1 / (duv1.x * duv2.y - duv1.y * duv2.x);
			vec3 tan = (dpos1 * duv2.y - dpos2 * duv1.y) * r;
			vec3 bitan = (dpos2 * duv1.x - dpos1 * duv2.x) * r;

			tanverts[indices[i + 0]].Tan += tan;
			tanverts[indices[i + 1]].Tan += tan;
			tanverts[indices[i + 2]].Tan += tan;
			tanverts[indices[i + 0]].Bitan += bitan;
			tanverts[indices[i + 1]].Bitan += bitan;
			tanverts[indices[i + 2]].Bitan += bitan;
		}

		for (size_t i = 0; i < vcount; i++)
		{
			tanverts[i].Tan = tanverts[i].Tan.normalized();
			tanverts[i].Bitan = tanverts[i].Bitan.normalized();
		}

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TanVert) * tanverts.size(), &tanverts[0], GL_STATIC_DRAW);

		glGenBuffers(1, &ebo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), &indices[0], GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, false, sizeof(TanVert), 0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(TanVert), BUFFER_OFFSET(sizeof(vec3)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, false, sizeof(TanVert), BUFFER_OFFSET(sizeof(vec3) * 2));
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 3, GL_FLOAT, false, sizeof(TanVert), BUFFER_OFFSET(sizeof(vec3) * 2 + sizeof(vec2)));
		glEnableVertexAttribArray(4);
		glVertexAttribPointer(4, 3, GL_FLOAT, false, sizeof(TanVert), BUFFER_OFFSET(sizeof(vec3) * 3 + sizeof(vec2)));

		glBindVertexArray(0);
	}

	Mesh::~Mesh()
	{
		glDeleteVertexArrays(1, &vao);
		glDeleteBuffers(1, &vbo);
		glDeleteBuffers(1, &ebo);
	}

	unsigned int Mesh::VAO() const
	{
		return vao;
	}

	int Mesh::ElementCount() const
	{
		return ecount;
	}
}