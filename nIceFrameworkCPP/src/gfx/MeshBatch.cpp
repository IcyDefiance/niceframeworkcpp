#include "stdafx.h"
#include "gfx/MeshBatch.h"
#include "gfx/DeferredTarget.h"
#include "gfx/Shader.h"
#include "util/File.h"
#include "Game.h"
#include "gfx/Camera.h"
#include "math/vec2.h"

using namespace std;

namespace nif
{
	class MeshBatchS
	{
	public:
		static DeferredTarget *gtarget;
		static Shader *gshader;
		static Shader *plshader;
		static Shader *alshader;
		static unsigned int vbo;
		static unsigned int vao;

		MeshBatchS()
		{
			gshader = new Shader(File::ReadAllText("shaders/geomV.glsl"), File::ReadAllText("shaders/geomF.glsl"));
			gshader->MakeActive();
			gshader->SetUniform("normMap", 1);
			gshader->SetUniform("specMap", 2);

			string lightV = File::ReadAllText("shaders/lightV.glsl");
			plshader = new Shader(lightV, File::ReadAllText("shaders/plightF.glsl"));
			plshader->MakeActive();
			plshader->SetUniform("position", 1);
			plshader->SetUniform("normal", 2);
			plshader->SetUniform("specular", 3);

			alshader = new Shader(lightV, File::ReadAllText("shaders/alightF.glsl"));

			auto tarSize = Game::Screen()->Size();
			gtarget = new DeferredTarget(tarSize.x, tarSize.y);

			vec2 quadPoints[4]{ vec2(-1, 1), vec2(-1, -1), vec2(1, 1), vec2(1, -1) };
			glGenVertexArrays(1, &vao);
			glGenBuffers(1, &vbo);
			glBindVertexArray(vao);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, sizeof(vec2) * 4, &quadPoints, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(vec2), 0);
			glBindVertexArray(0);
		}

		~MeshBatchS()
		{
			glDeleteVertexArrays(1, &vao);
			glDeleteBuffers(1, &vbo);
			delete gtarget;
			delete gshader;
			delete plshader;
			delete alshader;
		}
	};

#define S MeshBatchS

	DeferredTarget *S::gtarget;
	Shader *S::gshader;
	Shader *S::plshader;
	Shader *S::alshader;
	unsigned int S::vbo;
	unsigned int S::vao;

	MeshBatch::MeshBatch()
	{
		static S ctor;
	}

	MeshBatch::~MeshBatch()
	{
	}

	void MeshBatch::Add(const Model &model)
	{
		toDraw.push_back(&model);
	}

	void MeshBatch::Add(const PointLight &light)
	{
		pointLights.push_back(&light);
	}

	Camera cam(1440, 810, 1, 500, 90);
	void MeshBatch::Draw()
	{
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);

		Game::BindTarget(S::gtarget);
		glClearColor(0, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		S::gshader->MakeActive();
		S::gshader->SetUniform("view", cam.View());
		S::gshader->SetUniform("proj", cam.Projection());
		for (const Model *model : toDraw)
		{
			for (const Mesh *mesh : model->Meshes())
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, Texture::WhitePixel().Id());
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, Texture::DefaultNormal().Id());
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, Texture::DefaultSpecular().Id());

				S::gshader->SetUniform("world", mat4::translation(vec3(0, 0, -2)));
				glBindVertexArray(mesh->VAO());
				glDrawElements(GL_TRIANGLES, mesh->ElementCount(), GL_UNSIGNED_INT, 0);
			}
		}

		Game::BindTarget(nullptr);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindVertexArray(S::vao);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, S::gtarget->Diffuse().Id());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, S::gtarget->Position().Id());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, S::gtarget->Normal().Id());
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, S::gtarget->Specular().Id());

		S::alshader->MakeActive();
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

		S::plshader->MakeActive();
		for (const PointLight *light : pointLights)
		{
			S::plshader->SetUniform("campos", vec3(0));
			S::plshader->SetUniform("lightpos", light->Position);
			S::plshader->SetUniform("range", light->Range);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		toDraw.clear();
		pointLights.clear();
	}
}
