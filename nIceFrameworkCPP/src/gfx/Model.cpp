#include "stdafx.h"
#include "gfx/Model.h"
#include "tiny_obj_loader.h"
#include <Windows.h>
#include <stdexcept>
#include "util/StrExt.h"

using namespace std;
using namespace tinyobj;

namespace nif
{
	Model::Model(const string &filename)
	{
		vector<shape_t> shapes;
		vector<material_t> materials;

		string err;
		bool ret = tinyobj::LoadObj(shapes, materials, err, filename.c_str());

		if (!err.empty())
			OutputDebugString(StrExt::ToWString(err).c_str());

		if (!ret)
			throw runtime_error("Failed to load obj file");

		for (shape_t &shape : shapes)
		{
			auto mesh = shape.mesh;
			size_t vertCount = mesh.positions.size() / 3;

			meshes.push_back(new Mesh(mesh.positions, mesh.normals, mesh.texcoords, mesh.indices));
		}
	}

	Model::~Model()
	{
		for (Mesh *mesh : meshes)
			delete mesh;
	}

	const std::vector<Mesh*>& Model::Meshes() const
	{
		return meshes;
	}
}