#include "stdafx.h"
#include "gfx/P2U2.h"

namespace nif
{
	P2U2::P2U2(const vec2 position, const vec2 texcoord) : pos(position), uv(texcoord) { }
}