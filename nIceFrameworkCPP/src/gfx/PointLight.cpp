#include "stdafx.h"
#include "gfx/PointLight.h"

namespace nif
{
	PointLight::PointLight() { }
	PointLight::PointLight(const vec3 pos, const float range) : Position(pos), Range(range) { }
}