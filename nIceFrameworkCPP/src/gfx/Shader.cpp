#include "stdafx.h"
#include "gfx/Shader.h"
#include <Windows.h>
#include <GL/glew.h>
#include <stdexcept>
#include <vector>
#include "util/StrExt.h"

using namespace std;

namespace nif
{
	Shader::Shader(const string &vertSrc, const string &fragSrc)
	{
		auto cstr = vertSrc.c_str();
		auto clen = (int)vertSrc.length();
		vertShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertShader, 1, &cstr, &clen);
		glCompileShader(vertShader);
		if (!ShaderParam(vertShader, GL_COMPILE_STATUS))
			OutputDebugString(StrExt::ToWString("Failed to compile vertex shader: " + ShaderLog(vertShader) + "\r\n").c_str());

		cstr = fragSrc.c_str();
		clen = (int)fragSrc.length();
		fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragShader, 1, &cstr, &clen);
		glCompileShader(fragShader);
		if (!ShaderParam(fragShader, GL_COMPILE_STATUS))
			OutputDebugString(StrExt::ToWString("Failed to compile fragment shader: " + ShaderLog(fragShader) + "\r\n").c_str());

		program = glCreateProgram();
		glAttachShader(program, vertShader);
		glAttachShader(program, fragShader);
		glLinkProgram(program);
		if (!ProgramParam(program, GL_LINK_STATUS))
			OutputDebugString(StrExt::ToWString("Failed to compile shader program: " + ProgramLog(program) + "\r\n").c_str());
	}

	Shader::~Shader()
	{
		glDeleteProgram(program);
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
	}

	void Shader::MakeActive()
	{
		glUseProgram(program);
	}

	void Shader::SetUniform(const string &name, const float val)
	{
		glUniform1f(UniformLocation(name), val);
	}

	void Shader::SetUniform(const string &name, const int val)
	{
		glUniform1i(UniformLocation(name), val);
	}

	void Shader::SetUniform(const string &name, const vec2 &val)
	{
		glUniform2fv(UniformLocation(name), 1, reinterpret_cast<const  float*>(&val));
	}

	void Shader::SetUniform(const string &name, const ivec2 &val)
	{
		glUniform2iv(UniformLocation(name), 1, reinterpret_cast<const  int*>(&val));
	}

	void Shader::SetUniform(const string &name, const vec3 &val)
	{
		glUniform3fv(UniformLocation(name), 1, reinterpret_cast<const  float*>(&val));
	}

	void Shader::SetUniform(const string &name, const vec4 &val)
	{
		glUniform4fv(UniformLocation(name), 1, reinterpret_cast<const  float*>(&val));
	}

	void Shader::SetUniform(const std::string &name, const mat4 &val, const bool transpose)
	{
		glUniformMatrix4fv(UniformLocation(name), 1, transpose, reinterpret_cast<const  float*>(&val));
	}

	int Shader::UniformLocation(const string &name)
	{
		return glGetUniformLocation(program, name.c_str());
	}

	int Shader::ShaderParam(unsigned int shader, int param)
	{
		GLint value = -1;
		glGetShaderiv(shader, param, &value);
		return value;
	}

	int Shader::ProgramParam(unsigned int program, int param)
	{
		GLint value = -1;
		glGetProgramiv(program, param, &value);
		return value;
	}

	string Shader::ShaderLog(unsigned int shader)
	{
		int buflen = ShaderParam(shader, GL_INFO_LOG_LENGTH);
		string log(buflen, '\0');
		glGetShaderInfoLog(shader, buflen, &buflen, &log[0]);
		return log;
	}

	string Shader::ProgramLog(unsigned int program)
	{
		int buflen = ProgramParam(program, GL_INFO_LOG_LENGTH);
		string log(buflen, '\0');
		glGetProgramInfoLog(program, buflen, &buflen, &log[0]);
		return log;
	}
}