#include "stdafx.h"
#include "gfx/SpriteBatch.h"
#include "GL/glew.h"
#include "gfx/P2U2.h"
#include "macros.h"
#include "util\File.h"
#include "Game.h"

namespace nif
{
	class SpriteBatchS
	{
	public:
		static Shader *shader;
		static unsigned int vbo;
		static unsigned int vao;

		SpriteBatchS()
		{
			shader = new Shader(File::ReadAllText("shaders/spriteV.glsl"), File::ReadAllText("shaders/spriteF.glsl"));

			P2U2 verts[4] = {
				P2U2(vec2(0, 0), vec2(0, 0)),
				P2U2(vec2(0, -2), vec2(0, 1)),
				P2U2(vec2(2, 0), vec2(1, 0)),
				P2U2(vec2(2, -2), vec2(1, 1))
			};

			glGenBuffers(1, &vbo);
			glGenVertexArrays(1, &vao);
			glBindVertexArray(vao);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, sizeof(P2U2) * 4, verts, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(P2U2), BUFFER_OFFSET(0));
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(1, 2, GL_FLOAT, false, sizeof(P2U2), BUFFER_OFFSET(sizeof(vec2)));
		}

		~SpriteBatchS()
		{
			delete shader;
		}
	};

#define S SpriteBatchS

	Shader *S::shader;
	unsigned int S::vbo;
	unsigned int S::vao;

	SpriteBatch::SpriteBatch()
	{
		static S ctor;
	}

	SpriteBatch::~SpriteBatch()
	{
	}

	void SpriteBatch::Add(const Texture &texture, const vec2 position)
	{
		toDraw.push_back(Sprite(texture, position));
	}

	void SpriteBatch::Draw()
	{
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		S::shader->MakeActive();
		glBindVertexArray(S::vao);
		glActiveTexture(GL_TEXTURE0);

		for (Sprite &s : toDraw)
		{
			S::shader->SetUniform("pos", s.Position);
			S::shader->SetUniform("scale", vec2(1));
			S::shader->SetUniform("tint", vec4(1));
			S::shader->SetUniform("texSize", s.Source->Size());
			S::shader->SetUniform("tarSize", Game::Screen()->Size());
			glBindTexture(GL_TEXTURE_2D, s.Source->Id());
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		toDraw.clear();
	}

	SpriteBatch::Sprite::Sprite(const Texture &tex, const vec2 pos) : Source(&tex), Position(pos) { }
}