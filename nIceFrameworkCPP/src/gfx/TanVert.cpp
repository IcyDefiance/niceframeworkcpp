#include "stdafx.h"
#include "gfx/TanVert.h"

namespace nif
{
	TanVert::TanVert(vec3 pos, vec3 nor, vec2 uv, vec3 tan, vec3 bitan) : Pos(pos), Nor(nor), UV(uv), Tan(tan), Bitan(bitan) { }
}
