#include "stdafx.h"
#include "gfx/Texture.h"
#include "GL/glew.h"
#include "lodepng.h"

using namespace std;

namespace nif
{
	Texture::Texture(const int width, const int height)
	{
		Init(width, height);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
	}

	Texture::Texture(const int width, const int height, const vector<float> &data)
	{
		Init(width, height);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, &data[0]);
	}

	Texture::Texture(const std::string &filename)
	{
		vector<unsigned char> data;
		unsigned int width, height;
		lodepng::decode(data, width, height, filename);
		Init(width, height);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &data[0]);
	}

	Texture::~Texture()
	{
		glDeleteTextures(1, &id);
	}

	unsigned int Texture::Id() const
	{
		return id;
	}

	ivec2 Texture::Size() const
	{
		return size;
	}

	Texture& Texture::WhitePixel()
	{
		static Texture whitePixel(1, 1, vector<float>(4, 1));
		return whitePixel;
	}

	Texture & Texture::DefaultNormal()
	{
		static Texture defaultNormal(1, 1, vector<float> { .5f, .5f, 1, 1 });
		return defaultNormal;
	}

	Texture & Texture::DefaultSpecular()
	{
		static Texture defaultSpecular(1, 1, vector<float> { 1, 1, 1, .1f });
		return defaultSpecular;
	}

	void Texture::Init(const int width, const int height)
	{
		size = ivec2(width, height);

		glGenTextures(1, &id);
		glBindTexture(GL_TEXTURE_2D, id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}
}
