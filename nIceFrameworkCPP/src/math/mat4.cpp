#include "stdafx.h"
#include "math/mat4.h"
#include <math.h>

namespace nif
{
	mat4::mat4() : cols{ vec4(0), vec4(0), vec4(0), vec4(0) } { }
	mat4::mat4(const float v00, const float v01, const float v02, const float v03,
		const float v10, const float v11, const float v12, const float v13,
		const float v20, const float v21, const float v22, const float v23,
		const float v30, const float v31, const float v32, const float v33)
		: cols{ vec4(v00, v01, v02, v03), vec4(v10, v11, v12, v13), vec4(v20, v21, v22, v23), vec4(v30, v31, v32, v33) } { }
	mat4::mat4(const vec4 v0, const vec4 v1, const vec4 v2, const vec4 v3) : cols{ v0, v1, v2, v3 } { }

	mat4 mat4::translation(const vec3 &val)
	{
		return mat4(
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			val.x, val.y, val.z, 1);
	}

	mat4 mat4::lookat(const vec3 &eye, const vec3 &target, const vec3 &up)
	{
		vec3 zdir(eye - target);
		vec3 xdir = vec3::cross(up, zdir);
		vec3 ydir = vec3::cross(zdir, xdir);
		vec3 negeye = -eye;

		return mat4(
			xdir.x, ydir.x, zdir.x, 0.0f,
			xdir.y, ydir.y, zdir.y, 0.0f,
			xdir.z, ydir.z, zdir.z, 0.0f,
			vec3::dot(xdir, negeye), vec3::dot(ydir, negeye), vec3::dot(zdir, negeye), 1.0f);
	}

	mat4 mat4::perspectiveFov(const float fovy, const float width, const float height, const float near, const float far)
	{
		float yscale = 1 / tanf(fovy / 2);
		float xscale = yscale * height / width;
		float zscale = far / (near - far);

		return mat4(
			xscale, 0, 0, 0,
			0, yscale, 0, 0,
			0, 0, zscale, -1,
			0, 0, 2 * near * zscale, 0);
	}
}