#include "stdafx.h"
#include "util/File.h"
#include <fstream>
#include <cerrno>

using namespace std;

namespace nif
{
	namespace File
	{
		string ReadAllText(const string &filename)
		{
			ifstream in(filename, ios::in | ios::binary);
			if (!in)
				throw errno;

			string contents;
			in.seekg(0, ios::end);
			contents.resize(in.tellg());
			in.seekg(0, ios::beg);
			in.read(&contents[0], contents.size());
			in.close();
			return contents;
		}
	}
}