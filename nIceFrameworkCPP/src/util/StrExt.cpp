#include "stdafx.h"
#include "util/StrExt.h"

using namespace std;

namespace nif
{
	namespace StrExt
	{
		wstring ToWString(const string &in)
		{
			return wstring(in.begin(), in.end());
		}
	}
}
